"" vim-sublime - A minimal Sublime Text -like vim experience bundle
""               http://github.com/grigio/vim-sublime
"" Best view with a 256 color terminal and Powerline fonts


set nocompatible
filetype off
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
"" For better bundle
Plugin 'gmarik/vundle'
"" Great tool for auto-completion of variables and functions
Plugin 'Valloric/YouCompleteMe'
"" Fuzzy Search of files in repository|file directory. Super handy!!
Plugin 'kien/ctrlp.vim'
"" Pretty status bar
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
"" Git status extension (+-~ on the left)
Plugin 'airblade/vim-gitgutter'
"" Use git in vim. Great tool for diff, blame and other operations
Plugin 'tpope/vim-fugitive'
"" Good Auto Fill Tool use F2 to trigger
Plugin 'SirVer/ultisnips'
"" Great auto fill tool, use F2 to trigger and move next.
Plugin 'honza/vim-snippets'
"" Great tool for vim to search functions
Plugin 'majutsushi/tagbar'
"" These two are for mark-down
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'

"" C and other languages' formatting
Plugin 'Chiel92/vim-autoformat'
"" colorscheme Monokai_Medicine
Plugin 'crusoexia/vim-monokai'
"" Retro groove color scheme for Vim
Plugin 'morhetz/gruvbox'
"" Linux Coding Style
Plugin 'vivien/vim-linux-coding-style'
"" This plug-in provides automatic closing of quotes, parenthesis, brackets, etc.
Plugin 'Raimondi/delimitMate'
call vundle#end()
filetype plugin indent on
""""""""
if has('autocmd')
  filetype plugin on
  filetype plugin indent off
endif
if has('syntax') && !exists('g:syntax_on')
  syntax enable
endif

let g:gruvbox_italic=1
colorscheme gruvbox
"let g:syntastic_check_on_open = 1
"colorscheme monokai
"set t_Co=256
"let g:monokai_term_italic = 1
"let g:monokai_gui_italic = 1
"" Use :help 'option' to see the documentation for the given option.
"set autoindent
set complete-=i
set showmode

"set nrformats-=octal
set shiftround

set ttimeout
set ttimeoutlen=50

set incsearch

set laststatus=2
set ruler
set showcmd
set wildmenu

set encoding=utf-8
set tabstop=8 shiftwidth=8
set backspace=2
"set guifont=Go\ Mono\ for\ Powerline:s16

"" Use space instead of tab
""set et

"" Easier to delete space(tab)
set smarttab

inoremap <C-U> <C-G>u<C-U>

set number
set hlsearch

"" do not history when leavy buffer
set hidden

"so as not to be disturbed by Ctrl-S ctrl-Q in terminals:
"stty -ixon
noremap  <C-S> :update<CR>
vnoremap <C-S> <C-C>:update<CR>
inoremap <C-S> <Esc>:update<CR>
noremap  <C-E> :q!<CR>
vnoremap <C-E> <C-C>:q!<CR>
inoremap <C-E> <Esc>:q!<CR>

let mapleader = ','
nnoremap <Leader>p :set paste<CR>
nnoremap <Leader>o :set nopaste<CR>             
noremap  <Leader>g :GitGutterToggle<CR>

set noswapfile
set fileformats=unix,dos,mac

set cursorline
set completeopt=menuone,longest,preview
"" Open markdown files with Chrome.
autocmd BufEnter *.md exe 'noremap <F5> :!google-chrome-stable %:p<CR>'

"
" Plugins config
"

""" Markdown
let g:vim_markdown_folding_disabled=1
let g:vim_markdown_math=1

""" CtrlP
let g:ctrlp_working_path_mode = 'ra'
set wildignore+=*/.git/*,*/.hg/*,*/.svn/* 

""" You complete me
let g:ycm_global_ycm_extra_conf = '~/.vim/bundle/YouCompleteMe/cpp/ycm/.ycm_extra_conf.py'
let g:ycm_show_diagnostics_ui = 0
set completeopt-=preview
let g:ycm_min_num_of_chars_for_completion = 3
let g:ycm_min_num_identifier_candidate_chars = 3
let g:ycm_completion_confirm_key = '<Right>'
let g:ycm_autoclose_preview_window_after_completion = 1
let g:ycm_autoclose_preview_window_after_insertion = 1

""" Ultisnip
"" NOTE: <f1> otherwise it overrides <tab> forever
let g:UltiSnipsExpandTrigger="<F2>"
let g:UltiSnipsJumpForwardTrigger="<F2>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:did_UltiSnips_vim_after = 1

""" vim-airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
"" This can prevent the bug when only one tab left
let g:airline#extensions#tabline#show_buffers = 0
"" Show tab number by its sequence
let g:airline#extensions#tabline#tab_nr_type = 1
set statusline=%{GitBranch()}

" GitGutter

""" Tabs
let g:airline_theme='gruvbox'
let g:airline#extensions#tabline#enabled = 1
nnoremap <C-b>  :tabprevious<CR>
inoremap <C-b>  <Esc>:tabprevious<CR>i
"nnoremap <C-n>  :tabnext<CR>
"inoremap <C-n>  <Esc>:tabnext<CR>i

""" Chrome Secure Shell
nnoremap <C-h>  :tabnext<CR>
inoremap <C-h>  <Esc>:tabnext<CR>i

""" Switch btw splitted windows
nmap <silent> <A-Left> :wincmd w<CR>
nmap <silent> <A-Right> :wincmd w<CR>
" nnoremap <C-n>  :tabnew<CR>
" inoremap <C-n>  <Esc>:tabnew<CR>i
" nnoremap <C-k>  :tabclose<CR>
" inoremap <C-k>  <Esc>:tabclose<CR>i

"" this machine config
if filereadable(expand("~/.vimrc.local"))
  source ~/.vimrc.local
endif
"" this machine config
if filereadable(expand("~/.vimrc.mycolor"))
  set t_Co=256
  source ~/.vimrc.mycolor
endif

""" Tag bar
let g:tagbar_autofocus=1
let g:tagbar_foldlevel=2
map <F6> <ESC>:TagbarToggle<CR>
map <F10> <ESC>:set nu!<CR>

if has("cscope")
  set csprg=/usr/bin/cscope
  set csto=1
  set cst
  set nocsverb
  "" add any database in current directory
  if filereadable("cscope.out")
      cs add cscope.out
  endif
  set csverb
  """ Cscope
  "" Preprocess
  ""prototype for cscope.sh:
  "find ./dir1 ./dir2 -path ./ndir3 -prune  -iname '*.[ch]' > cscope.files
  nmap <F12> :!./cscope.sh;cscope -b -q<CR>:cs kill -1<CR>:cs add cscope.out<CR>

  "" Usage in Vim
  nmap <C-\>s :cs find s <C-R>=expand("<cword>")<CR><CR>
  nmap <C-\>g :cs find g <C-R>=expand("<cword>")<CR><CR>
  nmap <C-\>c :cs find c <C-R>=expand("<cword>")<CR><CR>
  nmap <C-\>t :cs find t <C-R>=expand("<cword>")<CR><CR>
  nmap <C-\>e :cs find e <C-R>=expand("<cword>")<CR><CR>
  nmap <C-\>f :cs find f <C-R>=expand("<cfile>")<CR><CR>
  nmap <C-\>i :cs find i <C-R>=expand("<cfile>")<CR><CR>
  nmap <C-\>d :cs find d <C-R>=expand("<cword>")<CR><CR>
  autocmd BufEnter *.c* exe 'noremap <F7> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q . && cscope -Rbkq<CR    >'

endif

""" vim-autoformat
let g:autoformat_autoindent = 0 
let g:autoformat_retab = 0 
let g:autoformat_remove_trailing_spaces = 0
let g:formatterpath = ['/usr/local/bin/clang-format', '/usr/local/bin/js-beautify']
"let g:autoformat_verbosemode = 1
"let g:formatdef_my_custom_json = '"js-beautify --config ~/.js-beautify"'
"let g:formatters_json = ['my_custom_json']
nmap == :Autoformat<CR>
autocmd BufEnter *.c* exe 'vmap = :Autoformat<CR>'
autocmd BufEnter *.json* exe 'vmap = :Autoformat<CR>'
"autocmd BufEnter *.c* exe 'nmap == :Autoformat<CR>'
"autocmd BufEnter *.c* exe 'nmap =<UP> :.-1,.Autoformat<CR>'
"autocmd BufEnter *.c* exe 'nmap =<DOWN> :.,.+1Autoformat<CR>'

""" Linux Coding Style takes an effect under dirs
let g:linuxsty_patterns = [ "/root"]

""" OSC52: Ctrl+c copy to clipboard in vim
source ~/.vim/osc52.vim
vmap <C-c> y:call SendViaOSC52(getreg('"'))<cr>

set virtualedit=onemore
vnoremap $ $h
